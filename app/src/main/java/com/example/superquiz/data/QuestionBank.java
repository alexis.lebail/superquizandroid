package com.example.superquiz.data;

import java.util.Arrays;
import java.util.List;

public class QuestionBank {
    public List<Question> getQuestions() {
        return Arrays.asList(
                new Question(
                        "Quelle est la capitale des ducs de Lorraine ?",
                        Arrays.asList(
                                "Nancy",
                                "Reims",
                                "Metz",
                                "La réponse D"
                        ),
                        0
                ),
                new Question(
                        "Où se situe le Parlement Européen ?",
                        Arrays.asList(
                                "Stockholm",
                                "Paris",
                                "Berlin",
                                "Strasbourg"
                        ),
                        3
                ),
                new Question(
                        "Quel est le chef-lieu du département du Calvados (14) ?",
                        Arrays.asList(
                                "Cabourg",
                                "Honfleur",
                                "Deauville",
                                "Caen"
                        ),
                        3
                ),
                new Question(
                        "Quelle épreuve de sport automobile a fêté ses 100 ans en 2023 ?",
                        Arrays.asList(
                                "Le Coupe de France",
                                "Les 24h du Mans",
                                "Le Grand Prix de Spa-Francorchamps",
                                "La réponse D"
                        ),
                        1
                ),
                new Question(
                        "Dans quel département se situe la ville du Mans ?",
                        Arrays.asList(
                                "Le Var",
                                "La Meuse (après tout, pourquoi pas !)",
                                "L'Eure",
                                "La Sarthe"
                        ),
                        3
                )
        );
    }
    private static QuestionBank instance;
    public static QuestionBank getInstance() {
        if (instance == null) {
            instance = new QuestionBank();
        }
        return instance;
    }
}